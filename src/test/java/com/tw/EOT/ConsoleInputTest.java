package com.tw.EOT;

import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ConsoleInputTest {

    @Test
    public void shouldInitializeConsoleInputInstance() {
        ConsoleInput playerInput = new ConsoleInput(new Scanner("0 1"));
        assertNotNull(playerInput);
    }

    @Test
    public void shouldReturnCheatMove() {
        ConsoleInput playerInput = new ConsoleInput(new Scanner("0"));
        assertEquals(Move.CHEAT, playerInput.nextMove());
    }

    @Test
    public void shouldReturnCooperateMove() {
        ConsoleInput playerInput = new ConsoleInput(new Scanner("1"));
        assertEquals(Move.COOPERATE, playerInput.nextMove());
    }
}
