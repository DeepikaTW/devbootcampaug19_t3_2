package com.tw.EOT;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

public class CopyCatBehaviourTest {
    private CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();

    @Test
    public void getInstance() {
        assertNotNull(copyCatBehaviour);
    }

    @Test
    public void shouldCopyCatReturnCooperateInFirstMove() {
        assertEquals(Move.COOPERATE, copyCatBehaviour.nextMove());
    }

    @Test
    public void shouldCopyCatReturnPreviousMoveOfTheOtherPlayer() {
        assertEquals(Move.COOPERATE, copyCatBehaviour.nextMove());

        copyCatBehaviour.update(mock(Game.class), new Move[]{Move.COOPERATE, Move.CHEAT});
        assertEquals(Move.CHEAT, copyCatBehaviour.nextMove());
    }

}
