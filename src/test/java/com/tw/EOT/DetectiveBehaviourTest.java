package com.tw.EOT;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;

public class DetectiveBehaviourTest {

    private static DetectiveBehaviour detectiveBehaviour;

    @Before
    public void shouldReturnInstance() {
        detectiveBehaviour = new DetectiveBehaviour();
    }

    @Test
    public void shouldRerturnFirstMove(){
        Assert.assertEquals(Move.COOPERATE,detectiveBehaviour.nextMove());
    }

    @Test
    public void ShouldTestNextThreeMove() {
        Assert.assertEquals(Move.COOPERATE,detectiveBehaviour.nextMove());
        Assert.assertEquals(Move.CHEAT,detectiveBehaviour.nextMove());
        Assert.assertEquals(Move.COOPERATE,detectiveBehaviour.nextMove());
        Assert.assertEquals(Move.COOPERATE,detectiveBehaviour.nextMove());
    }

    @Test
    public void shouldChangeBehaviourInFifthMove() {
        Assert.assertEquals(Move.COOPERATE,detectiveBehaviour.nextMove());
        detectiveBehaviour.update(mock(Game.class), new Move[]{Move.COOPERATE, Move.CHEAT});
        Assert.assertEquals(Move.CHEAT,detectiveBehaviour.nextMove());
        detectiveBehaviour.update(mock(Game.class), new Move[]{Move.CHEAT, Move.CHEAT});
        Assert.assertEquals(Move.COOPERATE,detectiveBehaviour.nextMove());
        detectiveBehaviour.update(mock(Game.class), new Move[]{Move.COOPERATE, Move.COOPERATE});
        Assert.assertEquals(Move.COOPERATE,detectiveBehaviour.nextMove());
        detectiveBehaviour.update(mock(Game.class), new Move[]{Move.COOPERATE, Move.COOPERATE});
        Assert.assertEquals(Move.COOPERATE,detectiveBehaviour.nextMove());
        detectiveBehaviour.update(mock(Game.class), new Move[]{Move.COOPERATE, Move.CHEAT});
        Assert.assertEquals(Move.COOPERATE,detectiveBehaviour.nextMove());
        detectiveBehaviour.update(mock(Game.class), new Move[]{Move.COOPERATE, Move.CHEAT});
        Assert.assertEquals(Move.CHEAT,detectiveBehaviour.nextMove());
    }


}
