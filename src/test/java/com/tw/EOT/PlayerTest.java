package com.tw.EOT;

import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.*;

public class PlayerTest {

    @Test
    public void shouldInitializePlayerInstance() {
        Player player = new Player(new ConsoleInput(new Scanner("0 1")));
        assertNotNull(player);
    }

    @Test
    public void shouldReturnCorrectScoreForOneRound() {
        Player player = new Player(new ConsoleInput(new Scanner("1")));
        player.updateScore(3);

        assertEquals(3, player.score());
    }

    @Test
    public void shouldReturnCorrectScoreForMulitpleRounds() {
        Player player = new Player(new ConsoleInput(new Scanner("1")));
        player.updateScore(3);
        player.updateScore(3);
        assertEquals(6, player.score());
    }
}
