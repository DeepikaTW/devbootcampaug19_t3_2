package com.tw.EOT;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MachineTest {

    Machine machine;

    @Before
    public void shouldInitializeMachineInstance() {
        machine = new Machine();
    }


//    @Test
//    public void shouldReturnZeroScoreForInvalidMove() {
//        Scores scores = machine.getScoreForTwoMoves(2, 4);
//        assertEquals(0, scores.getPlayer1Score());
//        assertEquals(0, scores.getPlayer2Score());
//    }

    @Test
    public void shouldReturnScoresForCheatCoperateMove() {
        Scores scores = machine.getScores(Move.CHEAT, Move.COOPERATE);
        assertEquals(3, scores.getPlayer1Score());
        assertEquals(-1, scores.getPlayer2Score());
    }

    @Test
    public void shouldReturnScoresForCooperateCoperateMove() {
        Scores scores = machine.getScores(Move.COOPERATE, Move.COOPERATE);
        assertEquals(2, scores.getPlayer1Score());
        assertEquals(2, scores.getPlayer2Score());
    }

    @Test
    public void shouldReturnScoresForCoperateCheatMove() {
        Scores scores = machine.getScores(Move.COOPERATE, Move.CHEAT);
        assertEquals(-1, scores.getPlayer1Score());
        assertEquals(3, scores.getPlayer2Score());
    }

    @Test
    public void shouldReturnScoresForCheatCheatMove() {
        Scores scores = machine.getScores(Move.CHEAT, Move.CHEAT);
        assertEquals(0, scores.getPlayer1Score());
        assertEquals(0, scores.getPlayer2Score());
    }
}
