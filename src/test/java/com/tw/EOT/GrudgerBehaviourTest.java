package com.tw.EOT;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class GrudgerBehaviourTest {
    private static GrudgerBehaviour grudgeBehaviour;

    @Before
    public void shouldReturnInstance() {
        grudgeBehaviour = new GrudgerBehaviour();
    }

    @Test
    public void shouldGrudgerReturnCooperateInFirstMove() {
        assertEquals(Move.COOPERATE, grudgeBehaviour.nextMove());
    }

    @Test
    public void shouldCopyCatReturnPreviousMoveOfTheOtherPlayer() {
        assertEquals(Move.COOPERATE, grudgeBehaviour.nextMove());

        grudgeBehaviour.update(mock(Game.class), new Move[]{Move.COOPERATE, Move.CHEAT});
        assertEquals(Move.CHEAT, grudgeBehaviour.nextMove());
    }
}
