package com.tw.EOT;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class GameTest {

    Player player1;
    Player player2;
    Machine machine;
    PrintStream outputStream;

    @Before
    public void setup() {
        player1 = Mockito.mock(Player.class);
        player2 = Mockito.mock(Player.class);
        machine = Mockito.mock(Machine.class);
        outputStream = Mockito.mock(PrintStream.class);
    }

    @Test
    public void shouldInitializeGameInstance() {
        Game game = new Game(null, null, null, 1, System.out);
        Assert.assertNotNull(game);
    }

    @Test
    public void verifyGameWasPlayedForOneRound() {
        when(player1.nextMove()).thenReturn(Move.CHEAT);
        when(player2.nextMove()).thenReturn(Move.CHEAT);
        when(machine.getScores(Move.CHEAT, Move.CHEAT)).thenReturn(new Scores(0, 0));
        when(player1.score()).thenReturn(0);
        when(player2.score()).thenReturn(0);
        Game game = new Game(player1, player2, machine, 1, outputStream);

        game.play();

        InOrder inOrder = Mockito.inOrder(player1, player2, machine, outputStream);
        inOrder.verify(player1).nextMove();
        inOrder.verify(player2).nextMove();
        inOrder.verify(machine).getScores(Move.CHEAT, Move.CHEAT);

        inOrder.verify(player1).updateScore(0);
        inOrder.verify(player2).updateScore(0);
        inOrder.verify(outputStream).println("Player 1 : " + 0 + ", Player 2 : " + 0);
    }

    @Test
    public void verifyGameWasPlayedForTwoRound() {
        when(player1.nextMove()).thenReturn(Move.CHEAT).thenReturn(Move.COOPERATE);
        when(player2.nextMove()).thenReturn(Move.CHEAT).thenReturn(Move.COOPERATE);
        when(player1.score()).thenReturn(0).thenReturn(2);
        when(player2.score()).thenReturn(0).thenReturn(2);
        when(machine.getScores(Move.CHEAT, Move.CHEAT)).thenReturn(new Scores(0, 0));
        when(machine.getScores(Move.COOPERATE, Move.COOPERATE)).thenReturn(new Scores(2, 2));
        Game game = new Game(player1, player2, machine, 2, outputStream);

        game.play();

        Mockito.verify(player1, times(2)).nextMove();
        Mockito.verify(player2, times(2)).nextMove();

        Mockito.verify(machine).getScores(Move.CHEAT, Move.CHEAT);
        Mockito.verify(machine).getScores(Move.COOPERATE, Move.COOPERATE);

        Mockito.verify(player1).updateScore(0);
        Mockito.verify(player2).updateScore(0);
        Mockito.verify(player1).updateScore(2);
        Mockito.verify(player2).updateScore(2);
        Mockito.verify(outputStream).println("Player 1 : " + 0 + ", Player 2 : " + 0);
        Mockito.verify(outputStream).println("Player 1 : " + 2 + ", Player 2 : " + 2);
    }

    @Test
    public void shouldNotifyCopyCatPlayerAboutMoves() {
        CopyCatBehaviour copyCatBehaviour = mock(CopyCatBehaviour.class);
        when(copyCatBehaviour.nextMove()).thenReturn(Move.COOPERATE);
        Player copyCatPlayer = new Player(copyCatBehaviour);
        when(player2.nextMove()).thenReturn(Move.CHEAT);
        when(player2.score()).thenReturn(0);
        when(machine.getScores(any(), any())).thenReturn(new Scores(0, 0));
        Game game = new Game(copyCatPlayer, player2, machine, 1, outputStream);
        game.addObserver(copyCatBehaviour);

        game.play();

        verify(copyCatBehaviour).update(game, new Move[]{Move.COOPERATE, Move.CHEAT});
    }

    @Test
    public void shouldNotifyTwoCopyCatPlayersAboutMoves() {
        CopyCatBehaviour copyCatBehaviour = mock(CopyCatBehaviour.class);
        CopyCatBehaviour copyCatBehaviour2 = mock(CopyCatBehaviour.class);
        when(copyCatBehaviour.nextMove()).thenReturn(Move.COOPERATE);
        when(copyCatBehaviour2.nextMove()).thenReturn(Move.COOPERATE);
        Player copyCatPlayer = new Player(copyCatBehaviour);
        Player copyCatPlayer2 = new Player(copyCatBehaviour2);
        when(copyCatPlayer.nextMove()).thenReturn(Move.COOPERATE);
        when(copyCatPlayer2.nextMove()).thenReturn(Move.COOPERATE);
        when(machine.getScores(any(), any())).thenReturn(new Scores(0, 0));
        Game game = new Game(copyCatPlayer, copyCatPlayer2, machine, 1, outputStream);
        game.addObserver(copyCatBehaviour);
        game.addObserver(copyCatBehaviour2);

        game.play();

        verify(copyCatBehaviour).update(game, new Move[]{Move.COOPERATE, Move.COOPERATE});
        verify(copyCatBehaviour2).update(game, new Move[]{Move.COOPERATE, Move.COOPERATE});
    }

    @Test
    public void shouldNotifyCopyCatPlayerAfterEveryRound() {
        CopyCatBehaviour copyCatBehaviour = mock(CopyCatBehaviour.class);
        when(copyCatBehaviour.nextMove()).thenReturn(Move.COOPERATE);
        Player copyCatPlayer = new Player(copyCatBehaviour);
        when(copyCatPlayer.nextMove()).thenReturn(Move.COOPERATE).thenReturn(Move.CHEAT);
        when(player2.nextMove()).thenReturn(Move.CHEAT);
        when(player2.score()).thenReturn(0);
        when(machine.getScores(any(), any())).thenReturn(new Scores(0, 0));
        Game game = new Game(copyCatPlayer, player2, machine, 2, outputStream);
        game.addObserver(copyCatBehaviour);

        game.play();

        verify(copyCatBehaviour).update(game, new Move[]{Move.COOPERATE, Move.CHEAT});
        verify(copyCatBehaviour).update(game, new Move[]{Move.CHEAT, Move.CHEAT});
    }
}
