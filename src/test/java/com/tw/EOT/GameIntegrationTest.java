package com.tw.EOT;

import org.junit.Assert;
import org.junit.Test;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

public class GameIntegrationTest {

    private PrintStream outputStream = mock(PrintStream.class);

    @Test
    public void shouldRunGameBetweenAlwaysCooperateAndAlwaysCheat() {
        Player player1 = new Player(PlayerInput.CHEATER);
        Player player2 = new Player(PlayerInput.COOPERATOR);
        Game game = new Game(player1, player2, new Machine(), 5, System.out);
        game.play();
        Assert.assertEquals(15,player1.score());
        Assert.assertEquals(-5,player2.score());
    }

    @Test
    public void shouldRunGameWithCopyCatAndCheater() {
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        Player copyCatPlayer = new Player(copyCatBehaviour);
        Player player2 = new Player(PlayerInput.CHEATER);
        Game game = new Game(copyCatPlayer, player2, new Machine(), 5, outputStream);
        game.addObserver(copyCatBehaviour);
        game.play();
        verify(outputStream, times(5)).println("Player 1 : -1, Player 2 : 3");
    }

    @Test
    public void shouldRunGameWithCopyCatAndGrudger() {
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        GrudgerBehaviour grudgerBehaviour = new GrudgerBehaviour();
        Player copyCatPlayer = new Player(copyCatBehaviour);
        Player grudgerPlayer = new Player(grudgerBehaviour);
        Game game = new Game(copyCatPlayer, grudgerPlayer, new Machine(), 5, outputStream);
        game.addObserver(copyCatBehaviour);
        game.addObserver(grudgerBehaviour);
        game.play();
        verify(outputStream).println("Player 1 : 2, Player 2 : 2");
        verify(outputStream).println("Player 1 : 4, Player 2 : 4");
        verify(outputStream).println("Player 1 : 6, Player 2 : 6");
        verify(outputStream).println("Player 1 : 8, Player 2 : 8");
        verify(outputStream).println("Player 1 : 10, Player 2 : 10");

    }
}
