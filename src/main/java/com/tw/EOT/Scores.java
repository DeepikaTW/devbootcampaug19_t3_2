package com.tw.EOT;

public class Scores {
    private int player1Score, player2Score;

    Scores(int player1Score, int player2Score) {
        this.player1Score = player1Score;
        this.player2Score = player2Score;
    }

    public int getPlayer1Score() {
        return this.player1Score;
    }

    public int getPlayer2Score() {
        return player2Score;
    }
}
