package com.tw.EOT;

import java.util.Observable;
import java.util.Observer;

public class CopyCatBehaviour implements PlayerInput, Observer {
    private Move currentMove = Move.COOPERATE;

    @Override
    public Move nextMove() {
        return currentMove;
    }

    @Override
    public void update(Observable o, Object moves) {
        Move[] previousMoves = (Move[]) moves;
        currentMove = currentMove == previousMoves[0] ? previousMoves[1] : previousMoves[0];
    }
}
