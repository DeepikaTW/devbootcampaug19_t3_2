package com.tw.EOT;

import java.io.PrintStream;
import java.util.Observable;
import java.util.Observer;

public class Game extends Observable {
    private final Player player1;
    private final Player player2;
    private final Machine machine;
    private final int noOfRounds;
    private final PrintStream outputStream;

    public Game(Player player1, Player player2, Machine machine, int noOfRounds, PrintStream outputStream) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.noOfRounds = noOfRounds;
        this.outputStream = outputStream;
    }

    public void play() {
        for (int i = 0; i < this.noOfRounds; i++) {
            Move player1Move = this.player1.nextMove();
            Move player2Move = this.player2.nextMove();
            Scores scores = this.machine.getScores(player1Move, player2Move);
            this.player1.updateScore(scores.getPlayer1Score());
            this.player2.updateScore(scores.getPlayer2Score());
            this.outputStream.println("Player 1 : " + player1.score() + ", Player 2 : " + player2.score());
            setChanged();
            notifyObservers(new Move[]{player1Move, player2Move});
        }
    }
}
