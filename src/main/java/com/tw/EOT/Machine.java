package com.tw.EOT;

public class Machine {

    public Scores getScores(Move moveOne, Move moveTwo) {
        if(moveOne == Move.COOPERATE && moveTwo == Move.COOPERATE) {
            return new Scores(2, 2);
        } else if(moveOne == Move.CHEAT && moveTwo == Move.COOPERATE) {
            return new Scores(3, -1);
        } else if(moveOne == Move.COOPERATE && moveTwo == Move.CHEAT) {
            return new Scores(-1, 3);
        }

        return new Scores(0, 0);
    }
}
