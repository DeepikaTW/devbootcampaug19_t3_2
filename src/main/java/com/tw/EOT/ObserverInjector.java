package com.tw.EOT;

import java.util.Observable;
import java.util.Observer;

public class ObserverInjector extends Observable {
    public void includeObservers(Observer observer) {
        this.addObserver(observer);
    }
}
