package com.tw.EOT;

import java.util.Scanner;

public class ConsoleInput implements PlayerInput {

    private final Scanner inputScanner;

    public ConsoleInput(Scanner scanner) {
        this.inputScanner = scanner;
    }

    public Move nextMove() {
        int nextMove = this.inputScanner.nextInt();

        if (nextMove == 1) {
            return Move.COOPERATE;
        }

        return Move.CHEAT;
    }
}