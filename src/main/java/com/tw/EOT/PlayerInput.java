package com.tw.EOT;

import java.util.Scanner;

public interface PlayerInput {

    PlayerInput CHEATER = () -> Move.CHEAT;
    PlayerInput COOPERATOR = () -> Move.COOPERATE;
    PlayerInput CONSOLE_INPUT = () -> new Scanner(System.in).nextInt() == 1 ? Move.COOPERATE : Move.CHEAT;

    public Move nextMove();
}
