package com.tw.EOT;

import java.util.Observable;
import java.util.Observer;

public class DetectiveBehaviour implements PlayerInput, Observer {

    private int numberOfRoundsPlayed = 0;
    private Move currMove = Move.COOPERATE;
    private int countOfCheat = 0;

    @Override
    public Move nextMove() {
        if(numberOfRoundsPlayed < 4) {
            currMove = (numberOfRoundsPlayed == 1) ? Move.CHEAT : Move.COOPERATE;
            numberOfRoundsPlayed++;
            return currMove;
        } else {
            if (countOfCheat > 1) {
               currMove =  new CopyCatBehaviour().nextMove();
            }
            return currMove;
        }
    }

    @Override
    public void update(Observable o, Object moves) {
        if(numberOfRoundsPlayed < 4) {
            Move[] previousMoves = (Move[]) moves;
            if ((previousMoves[0] == Move.CHEAT) || (previousMoves[1] == Move.CHEAT))
                countOfCheat++;
        } else {
            nextMove();
        }

    }
}
