package com.tw.EOT;

import java.util.Observable;
import java.util.Observer;

public class GrudgerBehaviour implements PlayerInput, Observer {

    private Move currentMove = Move.COOPERATE;

    public Move nextMove() {
        return currentMove;
    }

    @Override
    public void update(Observable o, Object moves) {
        Move[] previousMoves = (Move[]) moves;
        if((previousMoves[0]==Move.CHEAT)||(previousMoves[1]==Move.CHEAT))
        currentMove= Move.CHEAT;
        else
            currentMove=Move.COOPERATE;
    }
}
