package com.tw.EOT;

import java.util.Scanner;

public class Player {

    private final PlayerInput playerInput;
    private int score;

    public Player(PlayerInput playerInput) {
        this.playerInput = playerInput;
    }

    public Move nextMove() {
        return playerInput.nextMove();
    }

    public void updateScore(int score) {
        this.score += score;
    }

    public int score() {
        return this.score;
    }
}
