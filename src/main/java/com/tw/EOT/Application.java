package com.tw.EOT;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        Player player1 = new Player(new CopyCatBehaviour());
        Player player2 = new Player(new GrudgerBehaviour());
        Game game = new Game(player1, player2, new Machine(), 5, System.out);
        game.play();
    }

}
